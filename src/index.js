import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './assets/css/bootstrap.min.css';
import './assets/css/responsive.css';
import './assets/css/main.css';
import './assets/fonts/font-awesome/font-awesome.min.css';
import './assets/extras/animate.css';
import './index.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
