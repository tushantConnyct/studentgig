import React, { Component } from 'react';
import axios from 'axios';
import _ from 'lodash';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import logo from './student_gig_logo.png';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      category:'',
      data: []
    }
  }

  componentDidMount(){
    this.loadUniversityList();
  }

  loadUniversityList() {
     axios.get('http://www.student-gig.com:8080/SignUpRequest/University')
     .then((response) => {
        // return { options: response.data };
        this.setState({ data: response.data });
     })
    //  .then((json) => {
    //    return { options: json };
    //  })
     .catch((err) => {
        return { data: [] };
     })
  }


  onSelectRadioButton(event){
    this.setState({
      category: event.target.value
    });
  }

  onSubmit(event) {
    event.preventDefault();
    if (event.target.category.name === 'business') {
      const businessName = event.target.name.value;
    }
    const firstName = event.target.firstName.value;
    const lastName = event.target.lastName.value;
    const email = event.target.email.value;
    const comments = event.target.comment.value;
    // axios.post('http://www.student-gig.com:8080/SignUpRequest', {
    //   businessName: event.target.name === 'business' ? event.target.name.value,
    //   firstName: firstName,
    //   lastName: lastName,
    //   email: email,
    //   comment: comment
    // })
    // .then(function(response){
    //   console.log('saved successfully')
    // });

    axios({
    url: 'http://www.student-gig.com:8080/SignUpRequest',
    method: 'post',
    data:
     {
      businessName: event.target.name === 'business' ? event.target.name.value : '',
      contactType: this.state.category,
      firstName,
      lastName,
      email,
      comments
        }
        })
    .then(function(response){
      console.log('saved successfully')
    });
    event.target.firstName.value = ''
    event.target.lastName.value = ''
    event.target.email.value = ''
    event.target.comment.value = ''
    event.target.businessName.value = ''
  }

  handleChange(e) {
    console.log('e', e.target.value, e.target.id);
  }

  renderUniversity(data) {
    console.log('data', data);
    return (
      <option key={data.ID} id={data.ID} value={data.Name}>
       {data.Name}
      </option>
    )
  }

  render() {
    console.log('data', this.state.data.length);
    const options = _.map(this.state.data, this.renderUniversity);
    return (
      <div className="App">
        <div className="logo-menu">
          <nav className="navbar navbar-default navbar-fixed-top" role="navigation" data-spy="affix" data-offset-top="50">
            <div className="container">
              <div className="navbar-header col-md-3">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
                <a className="navbar-brand" href="#home">
                  <img src={logo} alt="student_gig" className="img-responsive logo" />
                </a>
              </div>

              <div className="collapse navbar-collapse" id="navbar">
                <ul className="nav navbar-nav col-md-9 pull-right">
                <li className="active"><a href="#hero-area"><i className="fa fa-home"></i> Home</a></li>
                <li><a href="#about"><i className="fa fa-info"></i> About</a></li>
                <li><a href="#contact"><i className="fa fa-envelope"></i> Contact</a></li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
        <section id="hero-area">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <h1 className="title"></h1>
                <div className="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-4 col-xs-12 animated fadeInRight delay-0-5">
                <a href="#" className="btn btn-common btn-lg text-center btn-center">Get Started</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="about">
          <div className="container">
          <div className="row">
          <h1 className="title">About us</h1>
          <h2 className="subtitle">Lorem Ipsum is simply dummy text</h2>
          <div className="col-md-8 col-md-offset-2 col-sm-12">
          <p>
            A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
          </p>
          </div>
          </div>
          </div>
        </section>
        <section id="contact">
          <div className="container text-center">
            <div className="row">
              <h1 className="title">Contact us</h1>
              <h2 className="subtitle">Get in touch with us</h2>
              <form role="form" className="contact-form" method="post" onSubmit={(event) => this.onSubmit(event)}>
                <div className="col-md-6 wow fadeInLeft" data-wow-delay=".5s">
                  <div className="form-group">
                    <div className="controls" onChange={(event) => this.onSelectRadioButton(event)}>
                      Are you a student or business?<br/>
                      <label className="radio-inline">
                        <input type="radio" name="category" value="student" />Student
                      </label>
                      <label className="radio-inline">
                        <input type="radio" name="category" value="business" />Business
                      </label>
                    </div>
                  </div>
                  {this.state.category === 'business' ?
                  <div className="form-group">
                    <div className="controls">
                      <input type="text" className="form-control" placeholder="Business" name="name" />
                    </div>
                  </div> : ''
                  }
                  <div className="form-group">
                    <div className="controls">
                      <input type="text" className="form-control" placeholder="First Name" name="firstName" />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="controls">
                      <input type="text" className="form-control" placeholder="Last Name" name="lastName" />
                    </div>
                  </div>
                  <div className="form-group">
                      <div className="controls">
                        <input type="email" className="form-control email" placeholder="Email" name="email"/>
                      </div>
                  </div>
                  <div className="form-group">
                    <div className="controls">
                      <select className="form-control" id="sel1" onChange={this.handleChange.bind(this)}>
                        {this.state.data.length === 0 ? <option value="loading...">loading</option> : options}
                      </select>
                      {/* <Select.Async
                        name="form-field-name"
                        value="one"
                        optionRenderer={this.loadUniversityList}
                      /> */}
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="controls">
                      <textarea rows="7" className="form-control" placeholder="Comment" name="comment"></textarea>
                    </div>
                  </div>
                  <button type="submit" id="submit" className="btn btn-lg btn-common">Send</button>
                </div>
              </form>
              <div className="col-md-6 wow fadeInRight">
                <div className="social-links">
                  <a className="social" href="#" target="_blank"><i className="fa fa-facebook fa-2x"></i></a>
                  <a className="social" href="#" target="_blank"><i className="fa fa-twitter fa-2x"></i></a>
                  <a className="social" href="#" target="_blank"><i className="fa fa-google-plus fa-2x"></i></a>
                  <a className="social" href="#" target="_blank"><i className="fa fa-linkedin fa-2x"></i></a>
                </div>
                <div className="contact-info">
                    <p><i className="fa fa-map-marker"></i> abcdef, 00000, USA</p>
                     <p><i className="fa fa-envelope"></i> info@yourwebsite.com</p>
                </div>
                <p>
                A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo lectus, ac blandit elit.<br/>
                </p>
              </div>
          </div>
          </div>
        </section>
        <div id="copyright">
          <div className="container">
            <div className="col-md-10"><p>© StudentGig 2017 All right reserved. Design & Developed by <a href="https://www.fiverr.com/tushantk">Tushant Khatiwada</a></p></div>
            <div className="col-md-2">
                <span className="to-top pull-right"><a href="#hero-area"><i className="fa fa-angle-up fa-2x"></i></a></span>
                </div>
            </div>
        </div>
      </div>
    );
  }
}

export default App;
